package de.hfu;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;
import de.hfu.integration.service.BaseResidentService;
import de.hfu.integration.service.ResidentServiceException;
import de.hfu.unit.Queue;
import de.hfu.unit.Util;
import org.junit.jupiter.api.*;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Beispiele für Unit-Tests
 */
@DisplayName("Test der Klasse App")
public class AppTest 
{

    /**
     * Wird einmal vor allen Tests dieser Klasse aufgerufen
     */
    @BeforeAll
    static void initAll() {

    }

    @Test
    void halbjahrTest()
    {
        for (int i = 1; i <= 6; i++) {
            assertTrue(Util.istErstesHalbjahr(i));
        }
        for (int i = 7; i <= 12; i++) {
            assertFalse(Util.istErstesHalbjahr(i));
        }
        assertThrows(IllegalArgumentException.class, () -> Util.istErstesHalbjahr(-1),"IllegalArgumentException was expected");
        assertThrows(IllegalArgumentException.class, () -> Util.istErstesHalbjahr(13),"IllegalArgumentException was expected");

        assertTrue(Util.istErstesHalbjahr(6));
        assertTrue(Util.istErstesHalbjahr(1));
        assertFalse(Util.istErstesHalbjahr(7));
        assertFalse(Util.istErstesHalbjahr(12));
    }
    @Test
    void queueTest(){
        assertThrows(IllegalArgumentException.class, () -> new Queue(0),"IllegalArgumentException was expected");

        Queue testQueue = new Queue(3);

        testQueue.enqueue(1);
        testQueue.enqueue(2);
        testQueue.enqueue(3);

        assertEquals(1, testQueue.dequeue());
        assertEquals(2, testQueue.dequeue()); //3

        testQueue.enqueue(4);   //3, 4
        testQueue.enqueue(5);   //3, 4, 5
        testQueue.enqueue(6);   //3, 4, 6        Letztes Element wird ersetzt

        assertEquals(3, testQueue.dequeue());
        assertEquals(4, testQueue.dequeue());
        assertEquals(6, testQueue.dequeue());
        assertThrows(IllegalStateException.class, () -> testQueue.dequeue(),"IllegalStateException was expected");


        testQueue.enqueue(1); //1
        testQueue.enqueue(2); //1, 2
        testQueue.enqueue(3); //1, 2, 3
        testQueue.enqueue(4); //1, 2, 4
        testQueue.enqueue(5); //1, 2, 5

        assertEquals(1, testQueue.dequeue());
        assertEquals(2, testQueue.dequeue());
        assertEquals(5, testQueue.dequeue());

        testQueue.enqueue(1); //1
        testQueue.enqueue(2); //1, 2
        testQueue.enqueue(3); //1, 2, 3

        assertEquals(1, testQueue.dequeue()); //2, 3
        testQueue.enqueue(7);   //2, 3, 7
        assertEquals(2, testQueue.dequeue());//3, 7
        assertEquals(3, testQueue.dequeue());//7
        assertEquals(7, testQueue.dequeue());


        testQueue.enqueue(1);
        testQueue.enqueue(testQueue.dequeue());

        assertEquals(1, testQueue.dequeue());
        assertThrows(IllegalStateException.class, () -> testQueue.dequeue(),"IllegalStateException was expected");

    }



   /* ResidentRepository stub = new ResidentRepositoryStub();
    @Test
    public void getUniqueResidentTest() {
        ResidentRepository stub = new ResidentRepositoryStub();
        BaseResidentService bRS = new BaseResidentService();
        bRS.setResidentRepository(stub);

        try {
            Resident unique = bRS.getUniqueResident(new Resident("", "", "", "", new Date(2001,10,10)));
            Assertions.assertEquals("Spongebob", unique.getGivenName(), "Date only - Given Name - spongebob");
        } catch(ResidentServiceException e) {
            fail("Exception thrown for Date only");
        } catch(Exception e) {
            fail("Wrongfully thrown Exception for Date only");
        }

        try {
            Resident unique = bRS.getUniqueResident(new Resident("spongebob", "", "", "", new Date(2001,10,10)));
            Assertions.assertEquals("Spongebob", unique.getGivenName(), "Date and Name - Given Name - spongebob");
            Assertions.assertEquals("Mart", unique.getFamilyName(), "Date and Name - Family Name - spongebob");
        } catch(ResidentServiceException e) {
            fail("Exception thrown for Date and Name");
        } catch(Exception e) {
            fail("Wrongfully thrown Exception for Date and Name");
        }

        try {
            Resident unique = bRS.getUniqueResident(new Resident("spongebob", "Mart", "Stutt", "Straße2", new Date(2001,10,10)));
            fail("Found Unique Resident, that doesn't exist");
        } catch(ResidentServiceException e) {
            Assertions.assertEquals(e.getMessage(), "Suchanfrage lieferte kein eindeutiges Ergebnis!", "All - Exception");
        } catch (Exception e) {
            fail("Wrongfully thrown Exception for All - Exception");
        }

        try {
            Resident unique = bRS.getUniqueResident(new Resident("spongebob", "Mart", "Straße1", "Stutt", new Date(2001,10,10)));
            Assertions.assertEquals("Mart", unique.getFamilyName(), "Date and Name - Family Name - spongebob");
        } catch(ResidentServiceException e) {
            fail("Exception thrown for All");
        } catch (Exception e) {
            fail("Wrongfully thrown Exception for All");
        }

        try {
            Resident unique = bRS.getUniqueResident(new Resident("Ger*", "", "", "", null));
            fail("Found Unique Resident, that doesn't exist");
        } catch(ResidentServiceException e) {
            Assertions.assertEquals(e.getMessage(), "Wildcards (*) sind nicht erlaubt!", "* - Exception");
        } catch (Exception e) {
            fail("Wrongfully thrown Exception for All - Exception");
        }
    }*/

    /**
     * Wird jeweils vor den Tests dieser Klasse aufgerufen
     */
    @BeforeEach
    void init() {

    }


    /**
     * Wird immer erfolgreich sein
     */
    @Test
    void succeedingTest() {
        //assertEquals(1, 1, "ist immer gleich");
    }

    /**
     * Wird niemals erfolgreich sein
     * Deshalb besser deaktiviert
     */
    @Test
    @Disabled("kein sinnvoller Test, deshalb deaktiviert")
    void failingTest() {
        fail("dieser Test schlägt fehl");
    }

    /**
     * Wird jeweils nach den Tests dieser Klasse aufgerufen
     */
    @AfterEach
    void tearDown() {
    }

    /**
     * Wird einmal nach allen Tests dieser Klasse aufgerufen
     */
    @AfterAll
    static void tearDownAll() {
    }

}